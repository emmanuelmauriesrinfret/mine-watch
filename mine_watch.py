# -*- coding: utf-8 -*-
"""
Created on Thu Dec 21 08:49:28 2017

@author: Emmanuel
"""

import socket
from time import sleep
import logging
from time import localtime, strftime
import subprocess
from pushover import Client
import mine_watch_config as cfg

def header():
    print (r'__/\\\\____________/\\\\__/\\\\\\\\\\\__/\\\\\_____/\\\__/\\\\\\\\\\\\\\\_        ')
    print (r' _\/\\\\\\________/\\\\\\_\/////\\\///__\/\\\\\\___\/\\\_\/\\\///////////__       ')
    print (r'  _\/\\\//\\\____/\\\//\\\_____\/\\\_____\/\\\/\\\__\/\\\_\/\\\_____________      ')
    print (r'   _\/\\\\///\\\/\\\/_\/\\\_____\/\\\_____\/\\\//\\\_\/\\\_\/\\\\\\\\\\\_____     ')
    print (r'    _\/\\\__\///\\\/___\/\\\_____\/\\\_____\/\\\\//\\\\/\\\_\/\\\///////______    ')
    print (r'     _\/\\\____\///_____\/\\\_____\/\\\_____\/\\\_\//\\\/\\\_\/\\\_____________   ')
    print (r'      _\/\\\_____________\/\\\_____\/\\\_____\/\\\__\//\\\\\\_\/\\\_____________  ')
    print (r'       _\/\\\_____________\/\\\__/\\\\\\\\\\\_\/\\\___\//\\\\\_\/\\\\\\\\\\\\\\\_ ')
    print (r'        _\///______________\///__\///////////__\///_____\/////__\///////////////__')
    print ('')
    print ('')
    print ('')
    print ('')

def time():
    time = '[' + strftime("%Y-%m-%d %H:%M:%S", localtime()) + '] '
    return time

def send_alert(msg):
    client = Client(cfg.user_key, api_token=cfg.api_token)
    client.send_message(msg, title='Miner Update')

def log_history(msg):
    f = open('log.txt', 'a')
    f.write(msg + "\n")
    f.close()
    
def check_temp(count):
    
    while True:
        for i in range(3):
            try:
                clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                clientsocket.connect(('localhost', 4068))
                clientsocket.send(bytes('threads', 'UTF-8'))
                API_raw = clientsocket.recv(1024).decode('utf-8')
                API_strip = API_raw.strip('|')
                API = API_strip[:-2]
                API = API.split("|")
                API = dict([i.strip() for i in item.split("=")] for item in API[0].split(";"))
                temp = float(API['TEMP'])
                print ( time() + 'Connecting...')
                sleep(5)
                break
            except TimeoutError:
                print ('Time Out - Retrying in 10s')
                sleep(10)
                continue
            except ConnectionRefusedError:
                print ('Connection Refused - Retrying in 10s')
                sleep(10)
                continue
        #If connection to API fails 3 times the script stop and wait for input before retrying
        else:
            send_alert('API Connection Error')
            input("Press Enter to continue...")
            continue
        break
        

    #Set temp treshold
    if temp < 55:
        msg = time() + 'ATTENTION - MINING SEEMS TO HAVE STOPPED - GPU Temp ' + str(temp)
        send_alert(msg)
        log_history(msg)
        logging.warning(msg)
        count += 1
        return count
    else:
        msg = time() + 'TO THE MOON!'
        print (msg)
        log_history(msg)
        if count == 1:
            send_alert(msg)
        count = 0
        return count
        

def mining_watch():
    print (path + ' ' + args)
    print ('MINING WATCH STARTING')
    miner_process = subprocess.Popen(path + ' ' + args, creationflags = subprocess.CREATE_NEW_CONSOLE)
    sleep(300)
    count = 0
    #Checks temp every 5min. If temp below treshold twice then restart miner
    while True:
        count = check_temp(count)
        if count == 2:
            msg = time() + 'RESTARTING MINER...'
            send_alert(msg)
            logging.warning(msg)
            log_history(msg)
            miner_process.terminate()
            sleep(10)
            miner_process = subprocess.Popen(path + ' ' + args, creationflags = subprocess.CREATE_NEW_CONSOLE)
            count = 0
        sleep(300)


global path
global args
path = cfg.path
args = cfg.args

header()
mining_watch()


