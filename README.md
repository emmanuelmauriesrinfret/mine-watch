# Python script watching CCminer

The script checks CCminer API every 5 mins. If GPU temperature is below treshold a notification is sent. If the temperature is still below treshold 5 min later then CCminer is restarted.

Add all files to your CCminer folder.

---

## Dependencies

you need to install the pushover python module <https://github.com/Thibauth/python-pushover>

You can install python-pushover from Pypi with:

    $ pip install python-pushover

You also need the pushover app and a pushover.net account.

Then you need to create a pushover application.

<https://pushover.net/apps/build>

---

## Update mine_watch_config.py

Update the config file.

---