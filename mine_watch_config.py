# -*- coding: utf-8 -*-
"""
Created on Tue Jan  9 12:30:17 2018

@author: Emmanuel
"""

#Pushover Client pushover.net
user_key = "your user key"
api_token = "your api_token"

#CCminer file path
path = "ccminer-x64.exe"
#CCminer config file name
args = "-c ccminer.conf"